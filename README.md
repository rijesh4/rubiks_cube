Project Title: Group Theory and its application in solving Rubik’s Cube 
Background:

Group Theory makes it possible to solve Rubik’s Cube mathematically, and is a worthy link enabling us to see most day to day problems, with an attitude of solving it mathematically. There are almost 2^12 * 3^8 * 8! *12! different possible configurations of a Rubik’s Cube (Chen 07). However, using Group Theory we can simulate a game of Rubik’s Cube (in C++) using only 6 moves.

 The most basic move one can do is to rotate a single face. We will let R denote a clockwise rotation of the right face (looking at the right face,
turn it 90 degree clockwise). Similarly, we will use the capital letters L, U, D, F, and B to denote clockwise twists of the corresponding faces. More generally, we will call any sequence of these 6 face twists a “move” of the Rubik’s cube.

 Rubik’s Cube is made up of 26 cubelets (small individual pieces of Rubik’s Cube). We know that the corner cubelets remain at any other, but, corner position. So we can divide the cubelets into two categories: Corner cubelets and Middle cubelets .


Statement of the Problem:

All the set of moves performed in the cube, denoted M = {R, L, U, D, F, B}, can be made into a Group. Basic properties of the group, Such as Associativity, Identity, and Inverse make it possible to keep track of the moves performed on the cube.  

In order to simulate a working C++ program of the Rubik’s Cube, we need to create two separate classes of Corner and Middle cubelets. The set of moves, being a Group, will keep track of the Corner and Middle cubelets, so what we need to do is to plot the data of value change of the different cubelets, when performed any move M*M.  

 Furthermore, we need to find ways to configure different cubelets i.e. to allocate position and orientation of each cubelets. Since, we can give each slot a number, like G = {1,2,3…n} , and make it into a group, which can be used into creating different permutations of the position of the slots to create an initial condition( : G->G’). Once the initial condition is set, we can randomly perform some moves M, so as to create the starting point of the game. Here, we need to create permutations, based on particular conditions, capable of resetting to a newer permutation when performed a move M. 
