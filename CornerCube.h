

#ifndef CUBE_H
#define CUBE_H

#include <iostream>
#include <string>
using namespace std;

class CornerCube{

public:
	
	

	CornerCube () {
		pre = "abc";
		head = "abc";
		tail = "abc";
	}

	CornerCube (string a, string b, string c) {
		
		pre = a;
		head = b; 
		tail = c;
	}

	void setValue (string a, string b, string c) {
		pre = a;
		head = b; 
		tail = c;
	}

	string getHead () {
		return head; 
	}

	string getTail (){
		return tail;
	}

	string getPre () {
		return pre;
	}

	void setHead (string a) {
		head = a;
	}

	void setTail (string a) {
		tail = a;
	}

	void setPre (string a) {
		pre = a;
	}

private:
	
	string pre;
	string head;
	string tail;

};


class MiddleCube {

public:
	MiddleCube () {
		pre = "abc";
		head = "abc";
	}

	MiddleCube (string a, string b) {
		pre = a;
		head = b; 
	}

	void setValue (string a, string b) {
		pre = a;
		head = b;
	}

	string getHead () {
		return head; 
	}

	string getPre () {
		return pre;
	}

	void setHead (string a) {
		head = a;
	}

	void setPre (string a) {
		pre = a;
	}

private:
	string head;
	string pre;
};

#endif