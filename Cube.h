

#include <iostream>

#include <string>
#include "CornerCube.h"
using namespace std;

class Cube {

public:

	
	Cube () {
		
		
		
			
	    	F = "F";
		T = "T";
		L = "L";
		R = "R";
		B = "B";
		K = "K";

		cubelets[0].setValue(L,F,T);
		cubelets[1].setValue(T,F,R);
		cubelets[2].setValue(L,F,B);
		cubelets[3].setValue(B,F,R);
		cubelets[4].setValue(R,K,T);
		cubelets[5].setValue(T,K,L);
		cubelets[6].setValue(R,K,B);
		cubelets[7].setValue(B,K,L);

		sidecubelets[0].setValue(T,F);
		sidecubelets[1].setValue(L,F);
		sidecubelets[2].setValue(B,F);
		sidecubelets[3].setValue(R,F);
		sidecubelets[4].setValue(T,K);
		sidecubelets[5].setValue(R,K);
		sidecubelets[6].setValue(B,K);
		sidecubelets[7].setValue(L,K);
		sidecubelets[8].setValue(T,L);
		sidecubelets[9].setValue(B,L);
		sidecubelets[10].setValue(T,R);
		sidecubelets[11].setValue(B,R);


	}

	void MoveRight () {
		
		CornerCube temp;
		temp = cubelets[7];

		cubelets[7] = cubelets [6];
		ShiftRight(cubelets [7]);

		cubelets[6] = cubelets [3];
		ShiftLeft(cubelets [6]);

		cubelets[3] = cubelets [2];
		ShiftRight(cubelets [3]);

		cubelets[2] = temp;
		ShiftLeft(cubelets[2]);

		MiddleCube temp1;
		temp1 = sidecubelets[2];
		sidecubelets[2] = sidecubelets[9];
		sidecubelets[9] = sidecubelets[6];
		sidecubelets[6] = sidecubelets[11];
		sidecubelets[11] = temp1;
	}

	void MoveUp () {
		
		CornerCube temp = cubelets[1];

		cubelets[1] = cubelets[3];
		Swap31(cubelets[1]);

		cubelets[3] = cubelets[6];
		ShiftLeft(cubelets[3]);

		cubelets[6] = cubelets[4];
		Swap46(cubelets[6]);
		
		cubelets[4] = temp;
		ShiftRight(cubelets[4]);
		
		MiddleCube temp1;
		temp1 = sidecubelets[3];
		sidecubelets[3] = sidecubelets[11];
		SwapHeadTail_MC (sidecubelets[3]);

		sidecubelets[11] = sidecubelets[5];
		SwapHeadTail_MC (sidecubelets[11]);

		sidecubelets[5] = sidecubelets[10];
		SwapHeadTail_MC (sidecubelets[5]);

		sidecubelets[10] = temp1;
		SwapHeadTail_MC (sidecubelets[10]);


	}

	void MoveTopRight() {
		
		CornerCube temp;
		temp = cubelets[1];

		cubelets[1] = cubelets [0];
		ShiftRight(cubelets [1]);

		cubelets[0] = cubelets [5];
		ShiftLeft(cubelets [0]);

		cubelets[5] = cubelets [4];
		ShiftRight(cubelets [5]);

		cubelets[4] = temp;
		ShiftLeft(cubelets[4]);

		MiddleCube temp1;
		temp1 = sidecubelets[0];
		sidecubelets[0] = sidecubelets[8];
		sidecubelets[8] = sidecubelets[4];
		sidecubelets[4] = sidecubelets[10];
		sidecubelets[10] = temp1;
	}

	void MoveLeftUp () {
		
		CornerCube temp = cubelets[0];

		cubelets[0] = cubelets[2];
		Swap46(cubelets[0]);

		cubelets[2] = cubelets[7];
		ShiftRight(cubelets[2]);

		cubelets[7] = cubelets[5];
		Swap31(cubelets[7]);
		
		cubelets[5] = temp;
		ShiftLeft(cubelets[5]);
		
		MiddleCube temp1;
		temp1 = sidecubelets[1];
		sidecubelets[1] = sidecubelets[9];
		SwapHeadTail_MC (sidecubelets[1]);

		sidecubelets[9] = sidecubelets[7];
		SwapHeadTail_MC (sidecubelets[9]);

		sidecubelets[7] = sidecubelets[8];
		SwapHeadTail_MC (sidecubelets[7]);

		sidecubelets[8] = temp1;
		SwapHeadTail_MC (sidecubelets[8]);

	}

	void SwapHeadTail_MC (MiddleCube &a) {
		string x;
		x = a.getHead();
		a.setHead (a.getPre());
		a.setPre(x);
	}

	void ShiftRight (CornerCube &a) { 
		
		string x;
		x = a.getTail () ;
		a.setTail(a.getHead());
		a.setHead(a.getPre());
		a.setPre(x);
	}

	void ShiftLeft (CornerCube &a) {
		string x;
		x = a.getPre();
		a.setPre(a.getHead());
		a.setHead (a.getTail());
		a.setTail(x);
	}

	
	void Swap31 (CornerCube &a) { // pre and Head Swap
		string x;
		x = a.getPre();
		a.setPre(a.getHead());
		a.setHead(x);
	}

	void Swap46 (CornerCube &a) { // head and Tail swap
		string x;
		x = a.getTail();
		a.setTail(a.getHead());
		a.setHead(x);
	}

	

	void PrintFront () {
		
		//createSpace();
		
		createSpace ();	
		cout<<cubelets [0].getHead() <<"  ";
		cout<<sidecubelets[0].getHead()<<" ";
		cout<<cubelets [1].getHead() <<"  ";
		cout<<endl;
		createSpace ();

		cout<<sidecubelets[1].getHead()<<" ";
		cout<<" F ";							//have to set the middle, but works here as "F" doesn't move			
		cout<<sidecubelets[3].getHead()<<" ";  
		cout<<endl;
		createSpace ();

		cout<<cubelets [2].getHead() <<"  ";
		cout<<sidecubelets[2].getHead()<<" ";
		cout<<cubelets [3].getHead() <<"  ";
		cout<<endl;

		
		//createSpace();

					
		
		

	}

	void createSpace () {
	
		cout<<"                                        ";
		}

private:

	CornerCube cubelets[8];
	MiddleCube sidecubelets [12];
	string F;
	string T;
	string L;
	string R;
	string B;
	string K;
}; 



